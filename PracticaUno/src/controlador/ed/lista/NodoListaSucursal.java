/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.lista;

/**
 *
 * @author cristian
 */
public class NodoListaSucursal<E> {
    
    private E info;
    private NodoListaSucursal sig;

    public NodoListaSucursal() {
        info = null;
        sig = null;
    }

    public NodoListaSucursal(E info, NodoListaSucursal sig) {
        this.info = info;
        this.sig = sig;
    }

    /**
     * @return the info
     */
    public E getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(E info) {
        this.info = info;
    }

    /**
     * @return the sig
     */
    public NodoListaSucursal getSig() {
        return sig;
    }

    /**
     * @param sig the sig to set
     */
    public void setSig(NodoListaSucursal sig) {
        this.sig = sig;
    }
    
    
    
}
